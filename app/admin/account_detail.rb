ActiveAdmin.register AccountDetail do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

controller do
  def permitted_params
    params.permit!
  end
end

form do |f|
    
  f.semantic_errors *f.object.errors.keys
  f.inputs do
  	f.input :payment_date, as: :datepicker
    f.input :payment_mode
    f.input :amount
    f.input :ref_no
    f.input :payment_copy

    f.actions
  end

end

end
