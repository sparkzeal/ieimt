ActiveAdmin.register AdminUser do
	controller do
		before_filter :authorized => :show

		private
		def authorized?(action, subject = nil)
			@admin = current_admin_user.admin
		 	if @admin == false
		   	action == :all
		  else
		  	true
		  end
	  end
	end
  permit_params :email, :password, :password_confirmation, :admin

  index do
    selectable_column
    id_column
    column :email
    column :admin
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count                                                                         
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :admin, as: :boolean
    end
    f.actions
  end

end
