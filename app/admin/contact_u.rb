ActiveAdmin.register ContactU do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
controller do
	before_filter :authorized => :show

	private
	def authorized?(action, subject = nil)
		@admin = current_admin_user.admin
	 	if @admin == false
	   	action == :all
	  else
	  	true
	  end
  end
end  

end
