ActiveAdmin.register Course do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
	# config.clear_action_items!
   # config.filters = false


	controller do
		before_filter :authorized => :show

		private
		def authorized?(action, subject = nil)
			@admin = current_admin_user.admin
		 	if @admin == false
		   	action == :all
		  else
		  	true
		  end
	  end
	  
	  def permitted_params
	    params.permit!
	  end
	end
	
	form do |f|
	  f.semantic_errors *f.object.errors.keys
	  f.inputs do
	    f.input :name,label: "Course Name : "
	    f.input :duration,label: "Course Duration : "
	    f.input :fee,label: "fee : "
	    f.input :minimum_qualification, as: :select, collection: ['10th', '12th','Diploma','Graduation','Post-Graduation', 'Phd', 'Others'], label: "Minimum Qualification : " 
		actions
		end
	end

end
