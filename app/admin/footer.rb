module ActiveAdmin
	module Views
		class Footer < Component

			def build
				super :id => "footer"
				super :style => "text-align: left;"

			end

		end
	end
end
