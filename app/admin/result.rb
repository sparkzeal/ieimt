ActiveAdmin.register Result do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
	# menu :if => proc{ can?(current_admin_user! ) }   
	# before_filter menu false if :valid
	sidebar "Result", only: [:new] do
    render :partial => 'search'
  end

	controller do
		before_filter :authorized => :show

	  def update_subjects

	  	# binding.pry
	  	# @student = Student.find_by enrolment_no: params[:student_id]
	  	@student =  Student.find_by enrolment_no: params[:q][:email_contains]
	  	@course = @student.course
	  	@subjects = @course.subjects

	  	# render  '_subjects' , :layout =>'active_admin'
	  end


		private
		def authorized?(action, subject = nil)
			@admin = current_admin_user.admin
		 	if @admin == false
		   	action == :all
		  else
		  	true
		  end
	  end
	  
	  def permitted_params
	    params.permit!
	  end
		# def valid
		# 	binding.pry
		# 	return true if !current_admin_user.admin?
		# end


	  # def create
	  # 	super
	  # end

	end

	form do |f|
	  
	  f.semantic_errors *f.object.errors.keys
	  f.inputs do
	  
	    # f.input :student_id, :input_html => { :value => 56 }, as: :string 
	    #:input_html => {:onchange => remote_request(:post, :update_subjects, { :student_id => "$('#result_student_id').val()"}, :subject_id)}
	    # , collection: Student.all.map{|s| [s.enrolment_no.to_s, s.id]}
	    # f.input :mode, label: "Mode of Exam : ",as: :select, collection: ['Distance', 'Regular']
	    f.input :certificate_no, label: "Certificate No. : "
	    if params[:action] == "edit"
	    	f.has_many :marks, heading: "Subject Marks" do |t|
	    		t.input :subject_id, as: :select, collection: Subject.all.map{|s| [s.name.to_s, s.id]}, :input_html => { :disabled => true } 
	    		t.input :marks_obtained
	    	end
	    else

	    	f.panel "Subjects" , id: 'mm' do
	    	end
	    end
		f.actions
		end
	end	

	index do
	  column :id
	  column "Student Name" do |result|
	  	if result.student.present?
	    	result.student.name
	  	end
	  end
	  column "Enrolment No." do |result|
	  	if result.student.present?
	    	result.student.enrolment_no
	  	end
	  end
	  column "Certificate Number", :certificate_no
	  actions
	end

	show do
		columns do
			column do
				panel "Result Details" do
					attributes_table_for result do
						row :certificate_no
					end
				end
				panel "Student Details" do
					attributes_table_for result.student do
						row "Enrolment No." do |s|
							s.enrolment_no
	          end
	          row "Student Name" do |s|
							s.name
	          end
	          row "Roll Number" do |s|
							s.roll_no
	          end
	        end
				end
			end
			column do
				panel "Subject Marks" do
					render :partial => 'show'
				end
			end
		end
	end

	filter :certificate_no
	filter :student_name, as: :string
	filter :created_at
	filter :updated_at

end
