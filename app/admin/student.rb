ActiveAdmin.register Student do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
# config.sort_order = 'id_asc'
	controller do
		before_filter :authorized => :show

		private
		def authorized?(action, subject = nil)
			@admin = current_admin_user.admin
		 	if @admin == false
		   	action == :all
		  else
		  	true
		  end
	  end

	  def permitted_params
	    params.permit!
	  end
	  # def create
	  # 	super
	  # 	binding.pry
	  # end
	end

	form do |f|
	    
	  f.semantic_errors *f.object.errors.keys
	  f.inputs do
	  	columns do
	  		column do
	  			panel "Course Detail" do
				    f.input :stream,label: "Stream/Branch : "
				    f.input :university_id, as: :select, collection: University.all, label: "Select University : " 
				  	f.input :course_id,label: "Select Course : ", as: :select, collection: Course.all
				  	f.input :session,label: "Session/Year : ", as: :string
				  	f.input :roll_no,label: "Roll Number :"
				  end
				  panel "Qualification Detail" do
				    f.input :last_exam_passed, as: :select,collection: ['10th', '12th','Diploma','Graduation','Post-Graduation', 'Phd', 'Others'], label: "Last Exam Passed : "
				    f.input :passing_year, label: "Passing Year : "
				  end
				end
				column do
					panel "Student Detail" do
				    f.input :name, label: "Student Name : "
				    f.input :father_name, label: "Father's Name : "
				    f.input :mother_name, label: "Mother's Name : "
				    f.input :dob, label: "Date of Birth : ", as: :datepicker, datepicker_options: { dateFormat: "d M yy", changeMonth: true, changeYear: true, yearRange: "1980:2005", defaultDate: '01 JAN 1980' }
				    f.input :gender, as: :select,collection: ['Male', 'Female'], label: "Gender : "
				    f.input :image, label: "Image Upload : "
				  end
				end
			end 

	    f.actions
	  end

	end

	index do
		column :id
		column :enrolment_no
	  column "Student Name", :name
	  column "Father name", :father_name
	  column "Course", :course
	  column "University" do |student|
	    student.university.name
	  end
	  
	  actions 
	end

	show do
		columns do
			column do
				panel "Course Details" do
					attributes_table_for student.university do
						row "University" do |u|
							u.name
	          end
					end
					attributes_table_for student.course do
						row "Course" do |c|
							c.name
	          end
					end
          attributes_table_for student do
						row :stream
						row :session
						row :roll_no
					end
				end
			end
			column do
				panel "Qualification Details" do
					attributes_table_for student do
						row :last_exam_passed
						row :passing_year
	        end
				end
			end
			column do
				panel "Student Deatails" do
					attributes_table_for student do
						row :name
						row :father_name
						row :mother_name
						row :dob
						row :gender
						row :image
					end
				end
			end
		end
	end

	filter :university_name, as: :select, collection: University.all
	filter :course_name, as: :select, collection: Course.all
	filter :enrolment_no
	filter :name
	filter :stream
	filter :roll_no
	filter :dob
	filter :session
	filter :created_at
	filter :updated_at

end
