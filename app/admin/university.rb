ActiveAdmin.register University do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

controller do
	before_filter :authorized => :show

	private
	def authorized?(action, subject = nil)
		@admin = current_admin_user.admin
	 	if @admin == false
	   	action == :all
	  else
	  	true
	  end
  end
	  
	def permitted_params
		params.permit!
	end
end

form do |f|
		
	f.semantic_errors *f.object.errors.keys
	f.inputs do
		f.input :name, label: "University Name"
		f.input :university_type, label: "University Type", as: :select, collection: ['Regular', 'Distance']
	f.actions
	end
end

index do
	column "id"
	column "University Name", :name
	column "University Type", :university_type
	actions
end

filter :name, label: "University Name"
filter :university_type, label: "University Type"
filter :created_at
filter :updated_at

end
