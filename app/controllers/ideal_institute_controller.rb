class IdealInstituteController < ApplicationController

	# def download
	# 	@client_ip = remote_ip()
 #  	send_file(
 #    	"#{Rails.root}/public/management.pdf",
 #    	filename: "management.pdf",
 #    	type: "application/pdf"
 # 	 	)
	# end

	def show_pdf
		if params[:course_type] == "management"
		  pdf_filename = File.join(Rails.root, "public/ieimt_management_details.pdf")
		  send_file(pdf_filename, :filename => "ieimt_management_details.pdf", :disposition => 'inline', :type => "application/pdf")
		end
		if params[:course_type] == "engineering"
		  pdf_filename = File.join(Rails.root, "public/ieimt_engineering_details.pdf")
		  send_file(pdf_filename, :filename => "ieimt_engineering_details.pdf", :disposition => 'inline', :type => "application/pdf")
		end
		if params[:admission_form] == "gcl_form"
		  pdf_filename = File.join(Rails.root, "public/form.pdf")
		  send_file(pdf_filename, :filename => "form.pdf", :disposition => 'inline', :type => "application/pdf")
		end
	end

	def universities
		# if params[:university_type] == "Regular University"
		# 	@universities = University.where university_type: "Regular University"
		# end
		# if params[:university_type] == "Distance University"
		# 	@universities = University.where university_type: "Distance University"
		# end
	end

	def contact_us
		@contact_us = ContactU.new
	end

	def contact_u
	 	@contact_us = ContactU.new(contact_params)
	 	if @contact_us.valid?
	 		@contact_us.save
	 		# ContactusMailer.contact_email(@contact_us).deliver_now
	 	end 
		redirect_to ideal_institute_contact_us_path
	end

	def marks_detail
		if !(params[:enrolment_no] && params[:dob]).blank?
			@student = Student.find_by enrolment_no: params[:enrolment_no]
			if @student.present? && @student.dob.to_time.strftime("%d/%m/%Y") == params[:dob].to_time.strftime("%d/%m/%Y")				
				@result = @student.result
				# if !@result.present?
				# 	return :back
				# end
			else
				redirect_to ideal_institute_verification_path
			end
		else
			redirect_to ideal_institute_verification_path
		end
	end

	private
  def contact_params
    params.require(:contact_u).permit(:name, :email, :phone_no, :city, :country, :message)
  end
end
