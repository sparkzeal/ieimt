class ContactusMailer < ApplicationMailer
   default from: 'notifications@example.com'
 
  def contact_email(contact_u)
	@contact_us = contact_u
	mail(to: 'info@ieimt.org', subject: 'Feedback/Query from ieimt.org')
  end
end
