class ContactU < ActiveRecord::Base
	validates :name,:email,:message,:phone_no, presence: true
	validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
end
