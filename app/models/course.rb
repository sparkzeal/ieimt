class Course < ActiveRecord::Base
	validates :name,:duration,:fee, presence: true
	has_many :subjects, :dependent => :destroy
	has_many :students, :dependent => :destroy
end
