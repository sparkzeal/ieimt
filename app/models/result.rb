class Result < ActiveRecord::Base
	validates :certificate_no, presence: true
	belongs_to :student
	has_many :marks, :dependent => :destroy
	has_many :subjects, through: :marks
	accepts_nested_attributes_for :marks
end
