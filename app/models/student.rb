class Student < ActiveRecord::Base
	validates :course_id,:university_id,:name,:dob,:gender,:session, presence: true
	belongs_to :university	
	belongs_to :course
	has_one :result, :dependent => :destroy
	before_create :generate_enrolment_no
	mount_uploader :image, ImageUploader

	protected
		def generate_enrolment_no
			begin
				base_code = SecureRandom.random_number
				# base_code.gsub!(/[^\w]|[iILloO01]/,'')
				code = base_code.to_s[2..5]
				code= "IEIMT-DEL"+code
				# binding.pry
			end while Student.where(enrolment_no: code).exists?
			self.enrolment_no = code
		end
end
