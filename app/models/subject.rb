class Subject < ActiveRecord::Base
	validates :name,:code, presence: true
	validates :code, uniqueness: true
	belongs_to :course
	has_many :marks, :dependent => :destroy
	has_many :results, through: :marks
	
end
