class University < ActiveRecord::Base
	validates :name,:university_type, presence: true
	has_many :students
end
