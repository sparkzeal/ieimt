Rails.application.routes.draw do
	devise_for :admin_users, ActiveAdmin::Devise.config
	match 'admin/results/update_subjects' => 'admin/results#update_subjects', via: [:get,:post], :as => :admin_update_subjects
	ActiveAdmin.routes(self)
	# The priority is based upon order of creation: first created -> highest priority.
	# See how all your routes lay out with "rake routes".

	# You can have the root of your site routed with "root"
	root 'ideal_institute#index'

	# Example of regular route:

		get 'ideal_institute/index' => 'ideal_institute#index'
		get 'ideal_institute/about-us' => 'ideal_institute#about-us'
		get 'ideal_institute/accreditations' => 'ideal_institute#accreditations'
		get 'ideal_institute/contact_us' => 'ideal_institute#contact_us'
		get 'ideal_institute/courses' => 'ideal_institute#courses'
		get 'ideal_institute/policy' => 'ideal_institute#policy'
		get 'ideal_institute/procedure' => 'ideal_institute#procedure'
		get 'ideal_institute/refund' => 'ideal_institute#refund'
		get 'ideal_institute/terms' => 'ideal_institute#terms'
		get 'ideal_institute/training-placement' => 'ideal_institute#training-placement'
		get 'ideal_institute/universities' => 'ideal_institute#universities'
		get 'ideal_institute/verification' => 'ideal_institute#verification'
		post 'ideal_institute/marks_detail' => 'ideal_institute#marks_detail'
		# get 'ideal_institute/download' => 'ideal_institute#download'
		get 'ideal_institute/show_pdf' => 'ideal_institute#show_pdf'
		match 'ideal_institute/contact_u' => 'ideal_institute#contact_u', via: [:get, :post]

	# Example of named route that can be invoked with purchase_url(id: product.id)
	#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

	# Example resource route (maps HTTP verbs to controller actions automatically):
	#   resources :products

	# Example resource route with options:
	#   resources :products do
	#     member do
	#       get 'short'
	#       post 'toggle'
	#     end
	#
	#     collection do
	#       get 'sold'
	#     end
	#   end

	# Example resource route with sub-resources:
	#   resources :products do
	#     resources :comments, :sales
	#     resource :seller
	#   end

	# Example resource route with more complex sub-resources:
	#   resources :products do
	#     resources :comments
	#     resources :sales do
	#       get 'recent', on: :collection
	#     end
	#   end

	# Example resource route with concerns:
	#   concern :toggleable do
	#     post 'toggle'
	#   end
	#   resources :posts, concerns: :toggleable
	#   resources :photos, concerns: :toggleable

	# Example resource route within a namespace:
	#   namespace :admin do
	#     # Directs /admin/products/* to Admin::ProductsController
	#     # (app/controllers/admin/products_controller.rb)
	#     resources :products
	#   end
end
