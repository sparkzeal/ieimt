class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string   :course
      t.string :stream
      t.string :session
      t.string :name
      t.string :father_name
      t.string :mother_name
      t.date :dob
      t.string :gender
      t.string :last_exam_passed
      t.string :passing_year

      t.timestamps null: false
    end
  end
end
