class CreateAccountDetails < ActiveRecord::Migration
  def change
    create_table :account_details do |t|
      t.date :payment_date
      t.string :payment_mode
      t.float :amount
      t.integer :ref_no

      t.timestamps null: false
    end
  end
end
