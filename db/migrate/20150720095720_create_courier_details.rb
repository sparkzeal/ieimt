class CreateCourierDetails < ActiveRecord::Migration
	def change
		create_table :courier_details do |t|
			t.string :product_name
			t.integer :ref_no
			t.date :dispached_on
			t.string :courier_company
			t.string :status

			t.timestamps null: false
		end
	end
end
