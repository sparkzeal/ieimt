class AddPaymentCopyToAccountDetails < ActiveRecord::Migration
  def change
    add_column :account_details, :payment_copy, :string
  end
end
