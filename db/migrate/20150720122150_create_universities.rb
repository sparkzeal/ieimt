class CreateUniversities < ActiveRecord::Migration
  def change
    create_table :universities do |t|
      t.string :name
      t.string :university_type

      t.timestamps null: false
    end
  end
end
