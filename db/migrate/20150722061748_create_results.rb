class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.integer :student_id
      t.string :certificate_no

      t.timestamps null: false
    end
  end
end
