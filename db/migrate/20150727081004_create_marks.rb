class CreateMarks < ActiveRecord::Migration
  def change
    create_table :marks do |t|
      t.integer :subject_id
      t.integer :result_id
      t.integer :marks_obtained

      t.timestamps null: false
    end
  end
end
