class AddColToMarks < ActiveRecord::Migration
  def change
    add_column :marks, :max_marks, :integer, :default => '100'
  end
end
