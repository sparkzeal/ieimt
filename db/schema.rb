# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150731071648) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_details", force: :cascade do |t|
    t.date     "payment_date"
    t.string   "payment_mode"
    t.float    "amount"
    t.integer  "ref_no"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "payment_copy"
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "contact_us", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "city"
    t.string   "country"
    t.string   "phone_no"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courier_details", force: :cascade do |t|
    t.string   "product_name"
    t.integer  "ref_no"
    t.date     "dispached_on"
    t.string   "courier_company"
    t.string   "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string   "name"
    t.string   "duration"
    t.float    "fee"
    t.string   "minimum_qualification"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "marks", force: :cascade do |t|
    t.integer  "subject_id"
    t.integer  "result_id"
    t.integer  "marks_obtained"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "max_marks",      default: 100
  end

  create_table "results", force: :cascade do |t|
    t.integer  "student_id"
    t.string   "certificate_no"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "students", force: :cascade do |t|
    t.string   "course"
    t.string   "stream"
    t.string   "session"
    t.string   "name"
    t.string   "father_name"
    t.string   "mother_name"
    t.date     "dob"
    t.string   "gender"
    t.string   "last_exam_passed"
    t.string   "passing_year"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "image"
    t.integer  "university_id"
    t.integer  "course_id"
    t.string   "enrolment_no"
    t.string   "roll_no"
  end

  create_table "subjects", force: :cascade do |t|
    t.string   "name"
    t.integer  "code"
    t.integer  "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "universities", force: :cascade do |t|
    t.string   "name"
    t.string   "university_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

end
