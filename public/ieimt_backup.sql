--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE account_details (
    id integer NOT NULL,
    payment_date date,
    payment_mode character varying,
    amount double precision,
    ref_no integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    payment_copy character varying
);


ALTER TABLE public.account_details OWNER TO postgres;

--
-- Name: account_details_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE account_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_details_id_seq OWNER TO postgres;

--
-- Name: account_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE account_details_id_seq OWNED BY account_details.id;


--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE active_admin_comments (
    id integer NOT NULL,
    namespace character varying,
    body text,
    resource_id character varying NOT NULL,
    resource_type character varying NOT NULL,
    author_id integer,
    author_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.active_admin_comments OWNER TO postgres;

--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE active_admin_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.active_admin_comments_id_seq OWNER TO postgres;

--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE active_admin_comments_id_seq OWNED BY active_admin_comments.id;


--
-- Name: admin_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE admin_users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    admin boolean DEFAULT false
);


ALTER TABLE public.admin_users OWNER TO postgres;

--
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_users_id_seq OWNER TO postgres;

--
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_users_id_seq OWNED BY admin_users.id;


--
-- Name: contact_us; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE contact_us (
    id integer NOT NULL,
    name character varying,
    email character varying,
    city character varying,
    country character varying,
    phone_no character varying,
    message text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.contact_us OWNER TO postgres;

--
-- Name: contact_us_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contact_us_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_us_id_seq OWNER TO postgres;

--
-- Name: contact_us_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contact_us_id_seq OWNED BY contact_us.id;


--
-- Name: courier_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE courier_details (
    id integer NOT NULL,
    product_name character varying,
    ref_no integer,
    dispached_on date,
    courier_company character varying,
    status character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.courier_details OWNER TO postgres;

--
-- Name: courier_details_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE courier_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courier_details_id_seq OWNER TO postgres;

--
-- Name: courier_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE courier_details_id_seq OWNED BY courier_details.id;


--
-- Name: courses; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE courses (
    id integer NOT NULL,
    name character varying,
    duration character varying,
    fee double precision,
    minimum_qualification character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.courses OWNER TO postgres;

--
-- Name: courses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE courses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courses_id_seq OWNER TO postgres;

--
-- Name: courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE courses_id_seq OWNED BY courses.id;


--
-- Name: marks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE marks (
    id integer NOT NULL,
    subject_id integer,
    result_id integer,
    marks_obtained integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    max_marks integer DEFAULT 100
);


ALTER TABLE public.marks OWNER TO postgres;

--
-- Name: marks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE marks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marks_id_seq OWNER TO postgres;

--
-- Name: marks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE marks_id_seq OWNED BY marks.id;


--
-- Name: results; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE results (
    id integer NOT NULL,
    student_id integer,
    certificate_no character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.results OWNER TO postgres;

--
-- Name: results_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.results_id_seq OWNER TO postgres;

--
-- Name: results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE results_id_seq OWNED BY results.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: students; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE students (
    id integer NOT NULL,
    course character varying,
    stream character varying,
    session character varying,
    name character varying,
    father_name character varying,
    mother_name character varying,
    dob date,
    gender character varying,
    last_exam_passed character varying,
    passing_year character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image character varying,
    university_id integer,
    course_id integer,
    enrolment_no character varying,
    roll_no character varying
);


ALTER TABLE public.students OWNER TO postgres;

--
-- Name: students_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE students_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.students_id_seq OWNER TO postgres;

--
-- Name: students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE students_id_seq OWNED BY students.id;


--
-- Name: subjects; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subjects (
    id integer NOT NULL,
    name character varying,
    code integer,
    course_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.subjects OWNER TO postgres;

--
-- Name: subjects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE subjects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subjects_id_seq OWNER TO postgres;

--
-- Name: subjects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE subjects_id_seq OWNED BY subjects.id;


--
-- Name: universities; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE universities (
    id integer NOT NULL,
    name character varying,
    university_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.universities OWNER TO postgres;

--
-- Name: universities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE universities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.universities_id_seq OWNER TO postgres;

--
-- Name: universities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE universities_id_seq OWNED BY universities.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY account_details ALTER COLUMN id SET DEFAULT nextval('account_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY active_admin_comments ALTER COLUMN id SET DEFAULT nextval('active_admin_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_users ALTER COLUMN id SET DEFAULT nextval('admin_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contact_us ALTER COLUMN id SET DEFAULT nextval('contact_us_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY courier_details ALTER COLUMN id SET DEFAULT nextval('courier_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY courses ALTER COLUMN id SET DEFAULT nextval('courses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY marks ALTER COLUMN id SET DEFAULT nextval('marks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY results ALTER COLUMN id SET DEFAULT nextval('results_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY students ALTER COLUMN id SET DEFAULT nextval('students_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subjects ALTER COLUMN id SET DEFAULT nextval('subjects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY universities ALTER COLUMN id SET DEFAULT nextval('universities_id_seq'::regclass);


--
-- Data for Name: account_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY account_details (id, payment_date, payment_mode, amount, ref_no, created_at, updated_at, payment_copy) FROM stdin;
1	2015-12-05	online	40000	1	2015-12-08 06:28:06.614743	2015-12-08 06:28:06.614743	\N
\.


--
-- Name: account_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('account_details_id_seq', 1, true);


--
-- Data for Name: active_admin_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY active_admin_comments (id, namespace, body, resource_id, resource_type, author_id, author_type, created_at, updated_at) FROM stdin;
\.


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('active_admin_comments_id_seq', 1, false);


--
-- Data for Name: admin_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin_users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, admin) FROM stdin;
2	singhnaresh004@gmail.com	$2a$10$aZ9JLXCH/CyTltwAlv5uMOfd23Z.TYG839JJ8bKnILJSZcmTYQG0C	\N	\N	\N	48	2016-06-14 11:32:56.159671	2016-06-13 12:14:22.82451	202.173.126.17	116.203.72.15	2015-12-01 10:21:59.461867	2016-06-14 11:32:56.163608	t
\.


--
-- Name: admin_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_users_id_seq', 4, true);


--
-- Data for Name: contact_us; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY contact_us (id, name, email, city, country, phone_no, message, created_at, updated_at) FROM stdin;
4	naresh	nareshsingh707@gmail.com	delhi	india	9212900683	hi	2016-02-18 10:36:25.433061	2016-02-18 10:36:25.433061
9	naresh	nareshsingh707@gmail.com	DELHI	INDIA	7291919110	HI	2016-03-30 08:10:58.157827	2016-03-30 08:10:58.157827
\.


--
-- Name: contact_us_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contact_us_id_seq', 9, true);


--
-- Data for Name: courier_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY courier_details (id, product_name, ref_no, dispached_on, courier_company, status, created_at, updated_at) FROM stdin;
\.


--
-- Name: courier_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('courier_details_id_seq', 1, false);


--
-- Data for Name: courses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY courses (id, name, duration, fee, minimum_qualification, created_at, updated_at) FROM stdin;
8	Advanced Diploma in Business Administration	2012-2014	38000	12th	2015-12-02 07:47:30.328961	2015-12-02 07:47:30.328961
9	Diploma in Business Administration	2012-2015	48000	12th	2015-12-02 07:49:33.193246	2015-12-02 07:49:33.193246
10	Bachelor Program in Business Administration	2012-2015	68900	12th	2015-12-02 07:50:53.87069	2015-12-02 07:50:53.87069
12	Master Program  in Business Administration 	2013-2015	58900	Graduation	2015-12-02 08:04:40.570186	2015-12-02 08:04:40.570186
13	Executive Master in Business Administration	2014-2015	68900	Graduation	2015-12-02 08:05:46.484042	2015-12-02 08:05:46.484042
14	Doctorate in Management Studies	2013-2015	78900	Graduation	2015-12-02 08:06:53.765921	2015-12-02 08:06:53.765921
11	Masters in Business Administration 	2013-2015	68900	Graduation	2015-12-02 08:03:12.839524	2015-12-02 08:07:25.279882
15	Diploma in Electrical Engineering	2012-2015	48900	10th	2015-12-02 09:27:29.777882	2015-12-02 09:27:29.777882
16	Diploma in Electronics Engineering	2012-2015	48900	10th	2015-12-02 09:28:23.370092	2015-12-02 09:28:23.370092
17	Diploma in Mechnical Engineering	2012-2015	48900	10th	2015-12-02 09:28:53.653042	2015-12-02 09:28:53.653042
18	Diploma in Civil Engineering	2012-2015	48900	10th	2015-12-02 09:29:24.339171	2015-12-02 09:29:24.339171
19	Diploma in Electronics  & Telecommunication  Engineering	2012-2015	48900	10th	2015-12-02 09:31:13.830382	2015-12-02 09:31:13.830382
20	Bachelor Program in Electrical  Engineering	2012-2015	68900	Diploma	2015-12-02 09:32:38.322095	2015-12-02 09:32:38.322095
21	Bachelor Program in Electronics  Engineering	2012-2015	68900	Diploma	2015-12-02 09:33:22.336489	2015-12-02 09:33:22.336489
22	Bachelor Program in Electronics & Telecommunication Engineering	2012-2015	68900	Diploma	2015-12-02 09:37:37.424171	2015-12-02 09:37:37.424171
23	Bachelor Program in Mechanical  Engineering	2012-2015	68900	Diploma	2015-12-02 09:38:56.532116	2015-12-02 09:38:56.532116
24	Bachelor Program in Civil Engineering	2012-2015	68900	Diploma	2015-12-02 09:39:35.557053	2015-12-02 09:39:35.557053
25	Master Program in Electrical Engineering	2013-2015	58900	Graduation	2015-12-02 09:41:24.454405	2015-12-02 09:41:24.454405
26	Master Program in Electronics Engineering	2013-2015	58900	Graduation	2015-12-02 09:42:29.5917	2015-12-02 09:42:29.5917
27	Master Program in Electronics & Telecommunication Engineering	2013-2015	58900	Graduation	2015-12-02 09:43:32.656989	2015-12-02 09:43:32.656989
28	Master Program in Mechanical Engineering	2013-2015	58900	Graduation	2015-12-02 09:44:54.925348	2015-12-02 09:44:54.925348
29	Master Program in Civil Engineering	2013-2015	58900	Graduation	2015-12-02 09:45:57.244509	2015-12-02 09:45:57.244509
30	Bachelor Program in Computer Application	2012-2015	68900	12th	2015-12-02 09:47:23.765273	2015-12-02 09:47:23.765273
31	Master Program in Computer Application	2013-2015	68900	Graduation	2015-12-02 09:48:16.611007	2015-12-02 09:48:16.611007
32	Post Graduate Diploma in Computer Application	2013-2015	68900	Graduation	2015-12-02 09:49:22.812509	2015-12-02 09:49:22.812509
33	Bachelor Program in Civil Engineering	2012-2015	68900	Diploma	2016-02-18 10:20:47.976138	2016-02-18 10:20:47.976138
\.


--
-- Name: courses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('courses_id_seq', 33, true);


--
-- Data for Name: marks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY marks (id, subject_id, result_id, marks_obtained, created_at, updated_at, max_marks) FROM stdin;
35	19	18	62	2016-06-08 07:31:10.028098	2016-06-08 07:31:10.028098	100
36	20	18	65	2016-06-08 07:31:10.031522	2016-06-08 07:31:10.031522	100
37	21	18	58	2016-06-08 07:31:10.033684	2016-06-08 07:31:10.033684	100
38	22	18	66	2016-06-08 07:31:10.035651	2016-06-08 07:31:10.035651	100
\.


--
-- Name: marks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('marks_id_seq', 38, true);


--
-- Data for Name: results; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY results (id, student_id, certificate_no, created_at, updated_at) FROM stdin;
14	10	IEIMT7031	2015-12-04 07:53:47.236088	2015-12-04 07:53:47.236088
18	11	IEIMT7032	2016-06-08 07:31:10.022678	2016-06-08 07:31:10.022678
\.


--
-- Name: results_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('results_id_seq', 18, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20150720080002
20150720080253
20150720080256
20150720095302
20150720095720
20150720100849
20150720101957
20150720122150
20150720125007
20150722050935
20150722053631
20150722054922
20150722061748
20150722071805
20150726090834
20150727081004
20150731071648
\.


--
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY students (id, course, stream, session, name, father_name, mother_name, dob, gender, last_exam_passed, passing_year, created_at, updated_at, image, university_id, course_id, enrolment_no, roll_no) FROM stdin;
11	\N		2012-2015	SAMBRE ASHISH KUNWARLAL	KUNWARLAL  MOHANLAL SAMBRE 	SAU SHOBHA KUNWARLAL SAMBRE	1970-12-31	Male	Diploma	2012	2015-12-04 08:05:41.805753	2016-02-22 07:17:26.36607	IMG-20151210-WA0002.jpg	4	24	IEIMT-DEL2090	DEL-156830
10	\N		2013-2016	SUMAN DEBNATH	NARAYAN DEBNATH	JYOTI RANI DEBNATH	1989-07-09	Male	10th	2008	2015-12-04 07:51:41.526401	2016-02-22 07:18:07.044219	2__1_.jpeg	4	18	IEIMT-DEL3330	DEL-156831
\.


--
-- Name: students_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('students_id_seq', 18, true);


--
-- Data for Name: subjects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY subjects (id, name, code, course_id, created_at, updated_at) FROM stdin;
19	Environmental Technology	621	24	2016-06-08 07:15:39.915562	2016-06-08 07:15:39.915562
20	CAD 	622	24	2016-06-08 07:16:37.392247	2016-06-08 07:16:37.392247
21	Major project	623	24	2016-06-08 07:17:22.254073	2016-06-08 07:17:22.254073
22	Viva	624	24	2016-06-08 07:17:54.84082	2016-06-08 07:17:54.84082
\.


--
-- Name: subjects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('subjects_id_seq', 22, true);


--
-- Data for Name: universities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY universities (id, name, university_type, created_at, updated_at) FROM stdin;
4	IEIMT	Distance	2016-02-18 10:07:20.645552	2016-02-18 10:07:20.645552
\.


--
-- Name: universities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('universities_id_seq', 5, true);


--
-- Name: account_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY account_details
    ADD CONSTRAINT account_details_pkey PRIMARY KEY (id);


--
-- Name: active_admin_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY active_admin_comments
    ADD CONSTRAINT active_admin_comments_pkey PRIMARY KEY (id);


--
-- Name: admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- Name: contact_us_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY contact_us
    ADD CONSTRAINT contact_us_pkey PRIMARY KEY (id);


--
-- Name: courier_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY courier_details
    ADD CONSTRAINT courier_details_pkey PRIMARY KEY (id);


--
-- Name: courses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (id);


--
-- Name: marks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY marks
    ADD CONSTRAINT marks_pkey PRIMARY KEY (id);


--
-- Name: results_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY results
    ADD CONSTRAINT results_pkey PRIMARY KEY (id);


--
-- Name: students_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY students
    ADD CONSTRAINT students_pkey PRIMARY KEY (id);


--
-- Name: subjects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subjects
    ADD CONSTRAINT subjects_pkey PRIMARY KEY (id);


--
-- Name: universities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY universities
    ADD CONSTRAINT universities_pkey PRIMARY KEY (id);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_namespace ON active_admin_comments USING btree (namespace);


--
-- Name: index_active_admin_comments_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_resource_type_and_resource_id ON active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_admin_users_on_email; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_email ON admin_users USING btree (email);


--
-- Name: index_admin_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_reset_password_token ON admin_users USING btree (reset_password_token);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

